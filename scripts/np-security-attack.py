import numpy as np
np.set_printoptions(precision=4)
import recommender as ET
import random

a = .3
honest_parties = 100
queries = 1000
honest_offers_per_query = 100
init_sybils = 2

def pick_proportional(U):
    return random.choices([u for (u, _) in U], weights = [t for (_, t) in U])[0]

def pick_best(U):
    best_trust = -1
    for (u, trust) in U:
        if trust > best_trust:
            best_trust = trust
            best_u = u
    return best_u

class Adv:
    def __init__(self, recom, init_sybils = init_sybils):
        self.recom = recom
        self.sybils = list(range(init_sybils))
        # bullet 1
        for i in range(init_sybils-1):
            recom.vote(i+1, 1, i)
        recom.vote(0, 1, init_sybils-1)

    # bullet 2
    def on_honest_add(self, i):
        assert i not in self.sybils
        assert self.recom.parties == i+1
        self.recom.vote(i+1, -1, i)
        self.sybils.append(i+1)

    # bullet 3
    def on_positive_vote(self, voter, votee):
        assert voter not in self.sybils
        assert votee not in self.sybils
        # choices may choose the same party twice, that's OK
        voters = random.choices(self.sybils, k = 2)
        self.recom.vote(voters[0], -1, voter)
        self.recom.vote(voters[1], -1, votee)

    # bullet 3
    def on_negative_vote(self, voter, votee):
        assert voter not in self.sybils
        assert votee in self.sybils
        # we're OK with self-votes and 2 votes from the same party
        voters = random.choices(self.sybils, k = 2)
        self.recom.vote(voters[0], -1, voter)
        self.recom.vote(voters[1], 1, votee)

pt = np.empty(0)

recom = ET.EigenTrust(pt, a, pick_proportional)
adv = Adv(recom)

for _ in range(honest_parties):
    new_party = recom.parties
    recom.add_party(new_party)
    adv.on_honest_add(new_party)

upvotes = 0
downvotes = 0
for _ in range(queries):
    honest_parties = list(set(range(recom.parties)) - set(adv.sybils))
    voter = random.choice(honest_parties)
    honest_offers = random.sample(honest_parties, k=honest_offers_per_query)
    chosen = recom.recommend(honest_offers + adv.sybils, voter)

    if chosen in honest_parties:
        recom.vote(voter, 1, chosen)
        adv.on_positive_vote(voter, chosen)
        upvotes += 1
    else:
        assert chosen in adv.sybils
        recom.vote(voter, -1, chosen)
        adv.on_negative_vote(voter, chosen)
        downvotes += 1

print("upvotes:", upvotes, "downvotes:", downvotes, "ratio:", downvotes/(upvotes+downvotes))
