from nltk.metrics.distance import jaro_similarity
from strings import strings
import sys

def distj(x, y):
    return 1-jaro_similarity(x, y)

for s1 in strings:
    for s2 in strings:
        for s3 in strings:
            if distj(s1, s3) > distj(s1, s2) + distj(s2, s3):
                print(s1, s2, s3, distj(s1, s3), distj(s1, s2), distj(s2, s3))
                print("Jaro distance is not a metric :(")
                sys.exit(0)
