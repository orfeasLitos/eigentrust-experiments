\section{Setting \& Definitions}
Here is a number of goals that the EigenTrust~\cite{DBLP:conf/www/KamvarSG03}
reputation system can aspire to fulfill. The properties get progressively
stricter and thus harder to achieve.

\subsection{Running Example}
As a running example, we will consider the case of using EigenTrust on top of a
music file sharing system. In this setting, Alice asks its peers to provide her
with a specific track. Out of all the offers, Alice chooses the peer recommended
by her reputation software. She then checks whether the downloaded file is
indeed the requested track and reports an up/downvote for this peer to the
reputation system accordingly.

We use this running example to aid intuition, but our definitions intend to be
application-agnostic. The setting of music sharing has been chosen as (i) there
is a degree of objectivity (either the received file is exactly the requested
track in good quality or it is not) and (ii) assuming that the music player
software is free of security vulnerabilities, the stakes if a bad file is
downloaded are low, as opposed to e.g.\ the setting of software sharing.

\subsection{Recommendation Provider}
We model an abstract \emph{recommendation provider} (RP) as an ideal
functionality. In other words, we assume a Trusted Third Party (TTP) provides
recommendations to and receives votes from all parties. In particular, an RP has
the interface of Fig.~\ref{code:rp-interface}. If some parties have fixed,
global trust values (commonly platform developers and other parties that are
deemed trustworthy by the community), their \emph{pre-trust} $\vec{p}$ is
provided as an \textsc{init} argument.

\begin{center}
  \begin{ifacebox}{Recommendation Provider}
    \begin{algorithmic}[1]
      \State \textsc{init}(pre-trust $\vec{p}$, $w$) \Comment{called only once
      at the beginning, $w$ can be of any type}
      \State $u$ sends (\textsc{post}, $(n, d)$) $\rightarrow$ store $(u, n, d)$
      \Comment{party, name, content}
      \State $u$ sends (\textsc{get}, $n$) $\rightarrow$ respond with party $v
      \in \{v | (v, n, \_) \text{ stored}\}$
      \State $u$ sends (\textsc{feed}, $v$) $\rightarrow$ respond with documents
      $P \subseteq \{(n, d) | (v, n, d) \text{ stored}\}$
      \State $u$ sends (\textsc{voteParty}, $v$, $b \in \{-1, 1\}$)
      \Comment{$v$ gives subjectively bad/good doc}
      \State $u$ sends (\textsc{voteDoc}, $(n, d)$, $b \in \{-1, 1\}$)
      \Comment{$(n, d)$ is a subjectively bad/good doc}
      \State \TODO{maybe add objective voting (``reporting'') for documents and
      parties}
    \end{algorithmic}
  \end{ifacebox}
  \captionof{figure}{Recommendation Provider Interface}
  \label{code:rp-interface}
\end{center}

In Fig.~\ref{code:rp-eigentrust}, we provide a concrete RP based on the
EigenTrust algorithm of Fig.~\ref{code:basic-eigentrust}. Pre-trust for both the
RP and the algorithm is normalized (i.e.\ $\sum p_u = 1$).

In Fig.~\ref{code:sdrp-interface}, we provide an interface in which only
documents are exposed (i.e.\ parties are not directly queried or voted) and that
assumes votes on documents based on subjective evaluations by parties (i.e.\
honest parties may disagree).

\begin{center}
  \begin{ifacebox}{Subjective Recommendation Provider}
    \begin{algorithmic}[1]
      \State \textsc{init}(pre-trust $\vec{p}$, $w$) \Comment{called only once
      at the beginning, $w$ can be of any type}
      \State $u$ sends (\textsc{rank}, set of documents $D$) $\rightarrow$
      respond with a ranking of $D$
      \State $u$ sends (\textsc{vote}, $d$, $b \in \{-1, 1\}$)
      \Comment{$d$ is a subjectively bad/good document}
    \end{algorithmic}
  \end{ifacebox}
  \captionof{figure}{Subjective, document-only Recommendation Provider Interface}
  \label{code:sdrp-interface}
\end{center}

\begin{center}
  \begin{algobox}{$\mathcal{ET}$(pre-trust $\vec{p}$, trust matrix $C$, weight
  $\alpha$)}
    \begin{algorithmic}[1]
      \State $t^{(0)} \gets \vec{p}$
      \State $t^{(k+1)} \gets (1-\alpha)C^Tt^{(k)} + \alpha \vec{p}$
      \State \Return $\lim\limits_{k \rightarrow \infty} t^{(k)}$
    \end{algorithmic}
  \end{algobox}
  \captionof{figure}{Basic EigenTrust algorithm~\cite{DBLP:conf/www/KamvarSG03}}
  \label{code:basic-eigentrust}
\end{center}

\begin{definition}[EigenTrust selector]
   \label{def:selector}
   A PPT algorithm $\mathcal{K}$ is an \emph{EigenTrust selector} if it takes as
   input a vector $\vec{q}$ of $(\text{party} \in \mathcal{P},
   \text{probability} \in [0, 1])$ pairs and returns one of the parties such
   that a party paired with a higher probability is more likely to be selected
   than a party paired with a lower probability (i.e.\ $\forall i, j \in
   [\text{len}(\vec{q})], \vec{q}_{i,2} > \vec{q}_{j,2} \Rightarrow
   \Pr[D(\vec{q}) = \vec{q}_{i,1}] > \Pr[D(\vec{q}) = \vec{q}_{j,1}]$.)
\end{definition}

\begin{center}
  \begin{systembox}{EigenTrust abstract implementation}
   \begin{algorithmic}[1]
     \State \textsc{init}(parties $\mathcal{P}$ with pre-trust $\vec{p} \in [0,
     1]^n$, $\alpha \in [0, 1]$):
     \Indent
       \State Initialize trust matrix $C$ (in an arbitrary fashion)
     \EndIndent
     \Statex
     \State On (\textsc{rank}, $D$) by $u$:
     \Indent
       \State Possibly do other actions
       \State $t \gets \mathcal{ET}(\vec{p}, C, \alpha)$ \Comment{$t$ may
       contain entries for parties, documents, etc.}
       \State \Return $D.\textsc{sortDescending}(d \mapsto \textsc{score}(d, t))$
       \Comment{\textsc{score} returns a $[0, 1]$ score for a document, based on
       the global trust vector}
     \EndIndent
     \Statex
     \State On (\textsc{vote}, $d$, $b \in \{-1, 1\}$) by $u$:
     \Indent
       \State Among possibly other actions, update $C$ (in an arbitrary fashion)
     \EndIndent
   \end{algorithmic}
  \end{systembox}
  \captionof{figure}{EigenTrust recommendation provider (partially specifies Subjective
  Recommendation Provider Interface -- Fig.~\ref{code:sdrp-interface})}
  \label{code:sdrp-eigentrust}
\end{center}

\begin{center}
  \begin{systembox}{Party-only EigenTrust}
   \begin{algorithmic}[1]
     \State \textsc{init}(parties $\mathcal{P}$ with pre-trust $\vec{p} \in [0,
     1]^n$, $\alpha \in [0, 1]$):
     \Indent
       \State $\vec{p}^{(0)} \gets \vec{p}$
       \ForAll{$u \in \mathcal{P}$}
         \State $\vec{c}_u \gets \vec{p}$ \Comment{initially everyone copies the
         pre-trust vector}
       \EndFor
       \State $D \gets \emptyset$ \Comment{set of documents}
     \EndIndent
     \Statex
     \State \textsc{addParty}($u$):
     \Indent
       \If{$u \notin \mathcal{P}$} \Comment{if new party}
         \State Add $u$ to $\mathcal{P}$ \Comment{$\mathcal{P} \gets \mathcal{P}
         \cup \{u\}$}
         \If{$\vec{p}^{(0)} \neq ()$} \Comment{initial pre-trust was not empty}
           \State $p_u = 0$ \Comment{new parties get no pre-trust}
         \Else \: \Comment{initial pre-trust was empty, $\vec{p}^{(0)} = ()$}
           \State $\vec{p} = (\underbrace{1/|\mathcal{P}|, \dots,
           1/|\mathcal{P}|}_{|\mathcal{P}|})$ \Comment{uniform pre-trust, c.f.\
           Subsec.\ 4.4, ``Basic EigenTrust''
           of~\cite{DBLP:conf/www/KamvarSG03}. Other strategies could be chosen
           here, but they could be exploited by \adversary as well.}
         \EndIf
         \ForAll{$v \in \mathcal{P}$}
           \State $c_{vu} = 0$ \Comment{the new party is initially untrusted}
         \EndFor
         \State $\vec{c}_u = \vec{p}$ \Comment{copy pre-trust vector for new
         party}
       \EndIf
     \EndIndent
     \Statex
     \State On (\textsc{post}, $(n, d)$) by $u$:
     \Indent
       \State Add party $u$ if unknown \Comment{\textsc{addParty}($u$)}
       \State \TODO{remove preexisting $(u, n, d')$?}
       \State Add $(u, n, d)$ to $D$ \Comment{$D \gets D \cup \{(u, n, d)\}$}
     \EndIndent
     \Statex
     \State On (\textsc{get}, $n$, EigenTrust selectors $\mathcal{K}_D,
     \mathcal{K}_P$) by $u$:
     \Indent
       \State Add party $u$ if unknown \Comment{\textsc{addParty}($u$)}
       \State Let $sc$ be a vector consisting of, for each content $d$ that has been
       posted with name $n$, the pair ($d$, sum of global trusts of posters of
       $(n, d)$) \Comment{$sc \gets \{d|(\_, n, d) \in D\}.\textsc{map}(d
       \rightarrow (d, \sum\limits_{v \in \mathcal{P}: (v, n, d) \in D} t_v))$}
       \State $d \gets \mathcal{K}_D(sc)$
       \State Let $q$ be a vector consisting of, for each party $v$ that has
       posted $(n, d)$, the pair ($v$, $t_v$) \Comment{$q \gets \{v'|(v', n, d)
       \in D\}.\textsc{map}(v \rightarrow (v, t_v))$}
       \State \Return $\mathcal{K}_P(q)$
     \EndIndent
     \Statex
     \State On (\textsc{vote}, $v$, $b \in \{-1, 1\}$) by $u$:
     \Indent
       \State Add unknown parties among $u, v$ \Comment{$\{u,
       v\}.\textsc{map}(\textsc{addParty})$}
       \State $c_{uv} \gets \max(c_{uv} + b, 0)$  \TODO{maybe $b \in \{-1, 1\}$
       is too big, consider scaling it down, e.g.\ $\pm0.05$, or more
       complicated vote weight}
       \If{$u$ trusts at least one party} \Comment{$\exists v \in \mathcal{P}:
       c_{uv} > 0$}
         \State Normalize $\vec{c}_u$ \Comment{$\forall v \in \mathcal{P},
         d_{uv} \gets \frac{c_{uv}}{\sum_{v \in \mathcal{P}} c_{uv}}; \vec{c}_u
         \gets \vec{d}_u$}
       \Else \: \Comment{$u$ trusts no one -- $\forall v \in \mathcal{P}, c_{uv}
       = 0$}
         \State $\vec{c}_u \gets \vec{p}$ \Comment {Revert to pre-trust, c.f.\
         Subsec.\ 4.5, ``Inactive Peers'' of~\cite{DBLP:conf/www/KamvarSG03}}
       \EndIf
       \State $\vec{t} \gets \mathcal{ET}(\vec{p}, C, \alpha)$
       \Comment{calculate trust vector}
     \EndIndent
   \end{algorithmic}
  \end{systembox}
  \captionof{figure}{EigenTrust recommendation provider}
  \label{code:rp-eigentrust}
\end{center}

\subsection{Execution}
Let $\extnats \triangleq \mathbb{N} \cup \{\infty\}$.

\begin{definition}[Subjective Execution]
\label{def:subj-ex}
Consider the collection of all possible documents $\mathcal{D}$. The execution
is parametrised by:
\begin{itemize}
  \item A set of parties $\mathcal{H}$. Each $u \in \mathcal{H}$ has an
  associated \emph{like map} $l_u : \mathcal{D} \mapsto \{-1, 1\}$,
  \item A machine that implements Fig.~\ref{code:sdrp-interface}, called SDRP,
  \item A pre-trust vector $\vec{p} \in [0, 1]^{|\mathcal{H}|}$,
  \item Auxiliary data $w$ that is compatible with the last input of the
  \textsc{init} function of SDRP,
  \item A message list consisting of elements:
  \begin{itemize}
  \item $(\textsc{rank}, D \subseteq \mathcal{D}, u \in \mathcal{H})$,
    \item $(\textsc{vote}, d \in \mathcal{D}, b = l_u(d), u \in
    \mathcal{H})$.
  \end{itemize}
\end{itemize}

The execution proceeds as follows:
\begin{itemize}
\item SDRP.$\textsc{init}(\vec{p}, w)$ is called.
\item For each entry in the message list, the corresponding message is sent to
the SDRP by the party included in the entry.
\end{itemize}

\TODO{this definition has no room for adversaries. Expand if needed}
\end{definition}

\subsection{Properties}
\begin{center}
  \begin{systembox}{Ideal Recommendation Provider}
    IRP lies on a spectrum from the most realistic, which starts with an all
    zeroed like map which is filled only for known votes, all the way to the
    perfect RP that knows the entire like map from initialization (is
    omniscient). In between lie RPs that update their belief on the like map at
    every vote and may or may not have some degree of omniscience. In order for
    these predictions to work, there has to be a distribution with interesting
    correlations over the like map, which is being learned through the voting
    process (c.f.\ Multi-armed bandit problem).
    \begin{algorithmic}[1]
      \State \textsc{init}($\vec{p}$):
      \Indent
        \State $\forall u \in \mathcal{H}, d \in \mathcal{D},$ set $l_u(d) = 0$
      \EndIndent
      \Statex
      \State On (\textsc{rank}, $D$) by $u$:
      \Indent
        \State \Return $D.\textsc{sortDescending}(d \mapsto l_u(d))$
        \Comment{non-deterministic on correctly sorted permutations}
      \EndIndent
      \Statex
      \State On (\textsc{vote}, $d$, $b$) by $u$:
      \Indent
        \State $l_u(d) = b$
      \EndIndent
    \end{algorithmic}
  \end{systembox}
  \captionof{figure}{Ideal Recommendation Provider for a Subjective Execution}
  \label{code:subj-ideal-rp}
\end{center}

\begin{definition}[No corruptions]
  \label{def:nc-sec}
  Let $q \in [0, 1]$ and ideal recommendation provider IRP (as in
  Fig.~\ref{code:subj-ideal-rp}). An SDRP is
  \emph{$q$-NC-re\-com\-mend\-er-secure} if, for any subjective execution
  (Def.~\ref{def:subj-ex}) with message list $L$ it is
  \begin{equation*}
    \min\limits_{(\textsc{rank}, D, u) \in L}(\min\limits_{D' \in
    \text{IRP}(\textsc{rank}, D, u)}dist_j(\text{SDRP}(\textsc{rank}, D,
    u), D')) \geq q \enspace,
  \end{equation*}
  where for $(m, u) \in L$, RP$(m, u)$ is the response of RP to the message $m$
  by $u$ during the execution and $dist_j$ is the Jaro
  distance~\cite{doi:10.1080/01621459.1989.10478785}. \orfeas{I went through the
  lists in \url{https://en.wikipedia.org/wiki/String_metric} and
  \url{https://www.coli.uni-saarland.de/courses/LT1/2011/slides/stringmetrics.pdf},
  apparently
  \url{https://en.wikipedia.org/wiki/Jaro\%E2\%80\%93Winkler_distance}~\cite{doi:10.1080/01621459.1989.10478785}
  fits well, also \url{https://en.wikipedia.org/wiki/Kendall_tau_distance},
  \url{https://en.wikipedia.org/wiki/Kendall_rank_correlation_coefficient} and
  \url{https://en.wikipedia.org/wiki/Spearman\%27s_rank_correlation_coefficient}
  are relevant} \TODO{improve response notation}
\end{definition}

Desired properties for an RP in the subjective setting:
\begin{itemize}
 \item Pre-trust only needed for bootstrapping, can be forgotten later
 \item A new honest party should be ``nudged'' towards the ``island'' of honest
 parties in relatively (in the number of Sybils/Sybil ``islands'') few
 query-vote cycles.
 \item A new Sybil party doesn't change the views of existing honest parties a
 lot.
 \item Malicious parties can cause less harm than good (for a suitable
 ``goodness'' metric). E.g.\ they have to offer a lot of value to honest parties
 in order to amass trust, all of which will be squandered by causing only a
 little harm.
\end{itemize}

The overarching theme of the desired properties for recommendation providers in
the objective setting is that a good reputation system minimizes bad
interactions. Our definitions are all based on the cryptographic security model,
in which honest parties follow the protocol exactly and all malicious parties
are controlled by a single Adversary \adversary that may behave in any arbitrary
manner. Each definition will progressively adopt a stronger security model.

\begin{definition}[Corruption ratio]
  Consider an execution as defined in TODO. For any moment in the execution, we
  define the \emph{corruption ratio} as $c \triangleq \frac{C}{C+H}$.
\end{definition}

\TODO{be more formal on total honest party downvotes}

\begin{definition}[Overt Sybils, honest pre-trusted parties]
  Let $q, s \in [0, 1]$. An RP is \emph{$(q, s)$-OSH-recommender-secure} if, for
  any PPT \adversary that can add corrupted parties and that immediately leaks
  such additions to all pre-trusted parties, such that at any point during the
  execution it is $c \leq s$ (where $c$ is the corruption ratio), the ratio of
  total honest party downvotes at any point of the exection is at most $q$.
\end{definition}

\begin{definition}[Covert Sybils, honest pre-trusted parties]
  Let $q, s \in [0, 1]$. An RP is \emph{$(q, s)$-CSH-recommender-secure} if for
  any PPT \adversary that can add corrupted parties (without pre-trusted parties
  learning that such parties are corrupted) such that at any point during the
  execution it is $c \leq s$ (where $c$ is the corruption ratio), the ratio of
  total honest party downvotes at any point of the exection is at most $q$.
\end{definition}

\begin{definition}[Covert corruptions, honest pre-trusted parties]
  Let $q, s \in [0, 1]$. An RP is \emph{$(q, s)$-CCH-recommender-secure} if for
  any PPT \adversary that can add corrupted parties (without pre-trusted parties
  learning that such parties are corrupted) and corrupt existing non-pre-trusted
  honest parties such that at any point during the execution it is $c \leq s$
  (where $c$ is the corruption ratio), the ratio of total honest party downvotes
  at any point of the exection is at most $q$.
\end{definition}

\TODO{Incorporate content (additionally to parties) in definitions}

\begin{definition}[Arbitrary covert corruptions]
  Let $q, s \in [0, 1]$. An RP is \emph{$(q, s)$-CA-recommender-secure} if for
  any PPT \adversary that can add corrupted parties and corrupt any honest party
  (without leaking such corruptions to any party) such that at any point during
  the execution it is $c \leq s$ (where $c$ is the corruption ratio), the ratio
  of total honest party downvotes at any point of the exection is at most $q$.
\end{definition}

\begin{definition}[No pre-trusted parties]
  \label{def:np-sec}
  Let $q, s \in [0, 1]$. An RP is \emph{$(q, s)$-NP-recommender-secure} if there
  are no pre-trusted parties (i.e.\ its \textsc{init}() is called with $\vec{p}
  = ()$) and for any PPT \adversary that can add corrupted parties and corrupt
  any honest party such that at any point during the execution it is $c \leq s$
  (where $c$ is the corruption ratio), the ratio of total honest party downvotes
  at any point of the exection is at most $q$.
\end{definition}

\TODO{decide if we want a bad-interactions-per-honest-party version of the
definitions.}
