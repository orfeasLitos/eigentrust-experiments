import numpy as np
from scipy.linalg import eig
np.set_printoptions(precision=4)

p = np.mat("0.;1.;0.;0.;0.;0.;0.;0.;0.;0.")
C = np.mat("""
    0. 1. 0. 0. 0. 0. 0. 0. 0. 0.;
    0.5 0. 0.5 0. 0. 0. 0. 0. 0. 0.;
    0. 0.166666 0. 0.166666 0. 0. 0.166666 0.166666 0.166666 0.166666;
    0. 0. 0.333333 0. 0.333333 0.333333 0. 0. 0. 0.;
    0. 0. 0. 0.2 0. 0. 0.2 0.2 0.2 0.2;
    0. 0. 0. 0.2 0. 0. 0.2 0.2 0.2 0.2;
    0. 0. 0.333333 0. 0.333333 0.333333 0. 0. 0. 0.;
    0. 0. 0.333333 0. 0.333333 0.333333 0. 0. 0. 0.;
    0. 0. 0.333333 0. 0.333333 0.333333 0. 0. 0. 0.;
    0. 0. 0.333333 0. 0.333333 0.333333 0. 0. 0. 0.
""")

for a in [0, 0.05, 0.1, 0.2, 0.3, 0.9, 1.]:
    t = p
    for _ in range(10000):
        t = (1-a)*C.T.dot(t) + a*p

    print(a)
    print(t)
    print()
