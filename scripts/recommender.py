import numpy as np

def et(pt, C, a, iters):
    res = pt
    for _ in range(iters):
        res = (1-a)*C.T.dot(res) + a*pt
    return res

class EigenTrust:
    def __init__(self, pt, a, d, iters=50):
        if pt.size != 0 and not np.isclose(sum(pt), 1):
            raise ValueError("p not normalized")
        self.pt = pt
        self.pt0 = pt
        self.C = np.array(np.repeat([pt], len(pt), axis=0))
        self.parties = pt.size
        self.a = a
        self.d = d
        self.iters = iters
        self.t = et(self.pt, self.C, self.a, self.iters)

    def add_party(self, u):
        if u < self.parties:
            return
        assert u == self.parties
        self.parties += 1
        if self.pt0.size != 0: # initial pre-trust non-empty
            self.pt = np.hstack([self.pt, np.array(0)]) # no pre-trust for new party
        else: # initial pre-trust empty
            self.pt = np.repeat(1/(self.parties), self.parties) # uniform pre-trust
        empty_column = np.zeros((self.parties-1, 1))
        self.C = np.concatenate((self.C, empty_column), axis=1)
        self.C = np.concatenate((self.C, np.array([self.pt])), axis=0)

    def new_party(self, u):
        return u >= self.parties

    def ensure_new_party_correctness(self, lst):
        new_parties = list(filter(self.new_party, lst))
        assert list(range(self.parties, self.parties+len(new_parties))) == new_parties

    def recommend(self, V, u):
        '''V: candidates, u: querying party'''
        self.ensure_new_party_correctness([u] + V)
        self.add_party(u)
        for v in V:
            self.add_party(v)
        return self.d([(v, self.t[v]) for v in V])

    def trusts_someone(self, u):
        for trust in self.C[u]:
            if trust > 0:
                return True
        return False

    def vote(self, v, b, u):
        '''v: votee, b: vote, u: voter'''
        self.ensure_new_party_correctness([u, v])
        if b not in [-1, 1]:
            raise ValueError("vote must be either -1 or 1")
        self.add_party(u)
        self.add_party(v)
        self.C[u,v] = max(self.C[u,v]+b, 0)
        if self.trusts_someone(u):
            denom = sum(self.C[u])
            self.C[u] = [trust/denom for trust in self.C[u]]
        else:
            self.C[u] = self.pt
        self.t = et(self.pt, self.C, self.a, self.iters)
